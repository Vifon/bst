/* File: tree.h */
/*************************************************************************/
/* Copyright (C) 2011  Wojciech Siewierski                               */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*************************************************************************/

#ifndef _TREE_H_
#define _TREE_H_

 #ifdef __cplusplus
 extern "C"
 {
 #endif


typedef struct tree
{
    void*        value;
    struct tree* parent;
    struct tree* left;
    struct tree* right;
} *Tree;

#define treeVal(A, T) (*(T*) (A)->value)
#define treeRef(A, T) ((T*) (A)->value)
#define newTreeNode() ((Tree) malloc(sizeof(struct tree)))

Tree treeInit    ();
Tree treeBegin   (Tree  root);
Tree treeRBegin  (Tree  root);
void treeFree    (Tree  root);
int  treeInsert  (Tree  root, void* val, int (*compare)(const void*, const void*));
Tree treeFind    (Tree  root, void* val, int (*compare)(const void*, const void*));
Tree treeFindMin (Tree  node);
Tree treeFindMax (Tree  node);
void treeRemove  (Tree* root, Tree iterator);
int  treeIsEmpty (Tree  root);
int  treeLength  (Tree  root);
Tree treeNext    (Tree  iterator);
Tree treePrev    (Tree  iterator);
int  treeSize    (Tree  root);
int  treeRRot    (Tree* root, Tree node);
int  treeLRot    (Tree* root, Tree node);
void treeForeach (Tree  root, void (*function)(void*, void*), void* arg);
/* Tree treeCopy    (Tree  source); */


 #ifdef __cplusplus
 }
 #endif
#endif
