/* File: tree.c */
/*************************************************************************/
/* Copyright (C) 2011  Wojciech Siewierski                               */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*************************************************************************/

#include "tree.h"
#include <stdlib.h>
#include <assert.h>

Tree treeInit()
{
    Tree root = newTreeNode();

    root->parent = NULL;
    root->left   = NULL;
    root->right  = NULL;
    root->value  = NULL;
    return root;
}

Tree treeBegin(Tree root)
{
    if (root->value == NULL)
        return NULL;
    else
        return treeFindMin(root);
}

Tree treeRBegin(Tree root)
{
    if (root->value == NULL)
        return NULL;
    else
        return treeFindMax(root);
}

void treeFree(Tree root)
{
    if (root == NULL)
        return;
    treeFree(root->left);
    treeFree(root->right);
    free(root);
}

/* compare should return <0 on lesser, 0 on equal and >0 on greater */
int treeInsert(Tree root, void* val, int (*compare)(const void*, const void*))
{
    if (root->value == NULL)
    {
        root->value = val;
        return 1;
    }
    else
    {
        int cmp = compare(root->value, val);
        if (cmp == 0)
            return 0;           /* element exists */
        else if (cmp > 0)
        {
            if (root->left)
                return treeInsert(root->left, val, compare);
            else
            {
                root->left         = newTreeNode();
                root->left->value  = val;
                root->left->parent = root;
                root->left->left   = NULL;
                root->left->right  = NULL;
                return 1;
            }
        }
        else /* (cmp < 0) */
        {
            if (root->right)
                return treeInsert(root->right, val, compare);
            else
            {
                root->right         = newTreeNode();
                root->right->value  = val;
                root->right->parent = root;
                root->right->left   = NULL;
                root->right->right  = NULL;
                return 1;
            }
        }
    }
}

/* compare should return <0 on lesser, 0 on equal and >0 on greater */
Tree treeFind(Tree root, void* val, int (*compare)(const void*, const void*))
{
    if (root == NULL || root->value == NULL)
        return NULL;
    else
    {
        int cmp = compare(root->value, val);
        if (cmp == 0)
            return root;
        else if (cmp > 0)
            return treeFind(root->left, val, compare);
        else /* (cmp < 0) */
            return treeFind(root->right, val, compare);
    }
}

Tree treeFindMin(Tree node)
{
    while (node->left != NULL)
        node = node->left;
    return node;
}

Tree treeFindMax(Tree node)
{
    while (node->right != NULL)
        node = node->right;
    return node;
}

void treeRemove(Tree* root, Tree iterator)
{
     if (iterator->right  == NULL &&
        iterator->left   == NULL &&
        iterator->parent == NULL)
    {
        iterator->value = NULL;
        return;
    }

   if (iterator->right == NULL)
    {
        if (iterator->parent != NULL)
        {
            if (iterator->parent->left == iterator)
                iterator->parent->left = iterator->left;
            else
                iterator->parent->right = iterator->left;
        }
        else
            *root = iterator->left;
        if (iterator->left)
            iterator->left->parent = iterator->parent;
    }
    else
    {
        Tree tmp = treeFindMin(iterator->right);
        if (iterator->parent != NULL)
        {
            if (iterator->parent->left == iterator)
                iterator->parent->left = tmp;
            else
                iterator->parent->right = tmp;
        }
        else
            *root = tmp;
        tmp->parent->left = tmp->right;
        tmp->parent = iterator->parent;
        tmp->left = iterator->left;
        if (iterator->left)
            iterator->left->parent = tmp;
        if (iterator->right)
            iterator->right->parent = tmp;
        tmp->right = iterator->right;
    }
    free(iterator);
}

int treeIsEmpty(Tree root)
{
    return (root->value == NULL &&
            root->left == NULL &&
            root->right == NULL &&
            root->parent == NULL);
}

Tree treeNext(Tree iterator)
{
    if (iterator->right)
        return treeFindMin(iterator->right);
    else
    {
        Tree tmp = iterator->parent;
        while (tmp && tmp->right == iterator)
        {
            iterator = iterator->parent;
            tmp      = tmp->parent;
        }
        return tmp;
    }
}

Tree treePrev(Tree iterator)
{
    if (iterator->left)
        return treeFindMax(iterator->left);
    else
    {
        Tree tmp = iterator->parent;
        while (tmp && tmp->left == iterator)
        {
            iterator = iterator->parent;
            tmp      = tmp->parent;
        }
        return tmp;
    }
}

int treeSize(Tree root)
{
    if (root == NULL)
        return 0;
    else
        return 1 + treeSize(root->left) + treeSize(root->right);
}

int treeRRot(Tree* root, Tree node)
{
    Tree tmp;
    if (node->left == NULL)
        return 0;

    tmp = node->left;

    node->left = node->left->right;
    tmp->right = node;
    tmp->parent = node->parent;
    node->parent = tmp;
    if (node->left)
        node->left->parent = node;

    if (tmp->parent)
    {
        if (tmp->parent->left == node)
            tmp->parent->left = tmp;
        else
            tmp->parent->right = tmp;
    }
    else
        *root = tmp;

    return 1;
}

int treeLRot(Tree* root, Tree node)
{
    Tree tmp;
    if (node->right == NULL)
        return 0;

    tmp = node->right;

    node->right = node->right->left;
    tmp->left = node;
    tmp->parent = node->parent;
    node->parent = tmp;
    if (node->right)
        node->right->parent = node;

    if (tmp->parent)
    {
        if (tmp->parent->right == node)
            tmp->parent->right = tmp;
        else
            tmp->parent->left = tmp;
    }
    else
        *root = tmp;

    return 1;
}

void treeForeach(Tree root, void (*function)(void*, void*), void* arg)
{
    Tree it;
    for (it = treeBegin(root); it != NULL; it = treeNext(it))
        function(it->value, arg);
}
