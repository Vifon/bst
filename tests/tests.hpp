// File: tests.hpp
#ifndef _TESTS_H_
#define _TESTS_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <regex.h>
#include "../src/tree.h"

class TreeTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(TreeTest);
    CPPUNIT_TEST(initialization);
    CPPUNIT_TEST(insert);
    CPPUNIT_TEST(find);
    CPPUNIT_TEST(remove);
    CPPUNIT_TEST(remove2);
    CPPUNIT_TEST(size);
    CPPUNIT_TEST(iterator);
    CPPUNIT_TEST(reverseIterator);
    CPPUNIT_TEST(rotations);
    CPPUNIT_TEST(parents);
    CPPUNIT_TEST(foreach);
    CPPUNIT_TEST_SUITE_END();
  public:
    void setUp();
    void tearDown();

  protected:
    void initialization();
    void insert();
    void find();
    void remove();
    void remove2();
    void size();
    void iterator();
    void reverseIterator();
    void rotations();
    void parents();
    void foreach();

  private:
    Tree t;
    int num[10];
    void fill();
};

#endif
