// File: tests.cpp
#include "tests.hpp"
#include <cstdlib>

CPPUNIT_TEST_SUITE_REGISTRATION(TreeTest);


static int cmpint(const void* a, const void* b)
{
    const int *A = reinterpret_cast<const int*>(a);
    const int *B = reinterpret_cast<const int*>(b);
    if (*A < *B)
        return -1;
    else if (*A > *B)
        return 1;
    else
        return 0;
}
void TreeTest::fill()
{
    for (int i = 0; i < 10; ++i)
        if (!treeInsert(t, num+i, cmpint))
        {
            std::cerr << std::endl << i << std::endl;
            exit(1);
        }
}


void TreeTest::setUp()
{
    t = treeInit();
    num[0] = 5;
    num[1] = 3;
    num[2] = 18;
    num[3] = -8;
    num[4] = 100;
    num[5] = 0;
    num[6] = 7;
    num[7] = 1;
    num[8] = 33;
    num[9] = 15;
}
/*
 *            5
 *            /\
 *          -/  \-
 *        -/      \-
 *       /          \
 *       3          18
 *       /          /\
 *      /          /  \
 *     /          /    \
 *    /          /      \
 *   -8          7      100
 *    \          |       |
 *     \          \     /
 *      \         |     |
 *      0         15   33
 *       \
 *       |
 *       1
 */

void TreeTest::tearDown()
{
    treeFree(t);
    t = NULL;
}

void TreeTest::initialization()
{
    CPPUNIT_ASSERT(t != NULL);
    CPPUNIT_ASSERT(t->left == NULL);
    CPPUNIT_ASSERT(t->right == NULL);
    CPPUNIT_ASSERT(t->parent == NULL);
    CPPUNIT_ASSERT(t->value == NULL);
}

void TreeTest::insert()
{
    fill();
    CPPUNIT_ASSERT(t->left->left->right->parent->parent->parent == t);
    CPPUNIT_ASSERT_EQUAL(5, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(3, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left->left, int));
    CPPUNIT_ASSERT(t->left->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->left->right, int));
    CPPUNIT_ASSERT(t->left->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(1, treeVal(t->left->left->right->right, int));
    CPPUNIT_ASSERT(t->left->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(7, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->left->right, int));
    CPPUNIT_ASSERT(t->right->left->right->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(33, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT(t->right->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->right->left->right == NULL);
}

void TreeTest::find()
{
    fill();
    int x = 33;
    int y = 1;
    int z = 13;
    CPPUNIT_ASSERT(treeFind(t, &x, cmpint) == t->right->right->left);
    CPPUNIT_ASSERT(treeFind(t, &y, cmpint) == t->left->left->right->right);
    CPPUNIT_ASSERT(treeFind(t, &z, cmpint) == NULL);
}

void TreeTest::remove()
{
    fill();
    int x = 5;
    int y = 3;
    int z = 18;

    treeRemove(&t, treeFind(t, &x, cmpint));

    CPPUNIT_ASSERT_EQUAL(7, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(3, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left->left, int));
    CPPUNIT_ASSERT(t->left->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->left->right, int));
    CPPUNIT_ASSERT(t->left->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(1, treeVal(t->left->left->right->right, int));
    CPPUNIT_ASSERT(t->left->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(33, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT(t->right->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->right->left->right == NULL);

    treeRemove(&t, treeFind(t, &y, cmpint));

    CPPUNIT_ASSERT_EQUAL(7, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->right, int));
    CPPUNIT_ASSERT(t->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(1, treeVal(t->left->right->right, int));
    CPPUNIT_ASSERT(t->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(33, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT(t->right->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->right->left->right == NULL);

    treeRemove(&t, treeFind(t, &z, cmpint));

    CPPUNIT_ASSERT_EQUAL(7, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->right, int));
    CPPUNIT_ASSERT(t->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(1, treeVal(t->left->right->right, int));
    CPPUNIT_ASSERT(t->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(33, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->left == NULL);
    CPPUNIT_ASSERT(t->right->right->right == NULL);
}

void TreeTest::remove2()
{
    int a = 9;
    int b = 12;

    treeInsert(t, (void*)&a, cmpint);
    treeInsert(t, (void*)&b, cmpint);

    CPPUNIT_ASSERT_EQUAL(9, treeVal(t, int));
    CPPUNIT_ASSERT(t->left == NULL);
    CPPUNIT_ASSERT_EQUAL(12, treeVal(t->right, int));
    CPPUNIT_ASSERT(t->right->left == NULL);
    CPPUNIT_ASSERT(t->right->right == NULL);

    treeRemove(&t, treeFind(t, &b, cmpint));
    CPPUNIT_ASSERT_EQUAL(9, treeVal(t, int));
    CPPUNIT_ASSERT(t->left == NULL);
    CPPUNIT_ASSERT(t->right == NULL);
}

void TreeTest::size()
{
    fill();
    CPPUNIT_ASSERT_EQUAL(10, treeSize(t));
}

void TreeTest::iterator()
{
    fill();

    int i = 0;
    for (Tree it1 = treeBegin(t), it2 = treeNext(it1);
         it1 != NULL && it2 != NULL;
         it1 = it2, it2 = treeNext(it2), ++i)
        CPPUNIT_ASSERT(treeVal(it1, int) < treeVal(it2, int));
    CPPUNIT_ASSERT_EQUAL(treeSize(t)-1, i);
}
void TreeTest::reverseIterator()
{
    fill();

    int i = 0;
    for (Tree it1 = treeRBegin(t), it2 = treePrev(it1);
         it1 != NULL && it2 != NULL;
         it1 = it2, it2 = treePrev(it2), ++i)
        CPPUNIT_ASSERT(treeVal(it1, int) > treeVal(it2, int));
    CPPUNIT_ASSERT_EQUAL(treeSize(t)-1, i);
}

void TreeTest::rotations()
{
    fill();

    CPPUNIT_ASSERT(treeRRot(&t, t->right));
    CPPUNIT_ASSERT_EQUAL(7, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right->right, int));
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right->right, int));

    CPPUNIT_ASSERT(treeLRot(&t, t->right));
    CPPUNIT_ASSERT(t->left->left->right->parent->parent->parent == t);
    CPPUNIT_ASSERT_EQUAL(5, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(3, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left->left, int));
    CPPUNIT_ASSERT(t->left->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->left->right, int));
    CPPUNIT_ASSERT(t->left->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(1, treeVal(t->left->left->right->right, int));
    CPPUNIT_ASSERT(t->left->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(7, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(15, treeVal(t->right->left->right, int));
    CPPUNIT_ASSERT(t->right->left->right->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(100, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(33, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT(t->right->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->right->left->right == NULL);

    CPPUNIT_ASSERT(treeRRot(&t, t->left->left) == 0);

    CPPUNIT_ASSERT(treeRRot(&t, t));
    CPPUNIT_ASSERT_EQUAL(3, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(-8, treeVal(t->left, int));
    CPPUNIT_ASSERT_EQUAL(5, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(18, treeVal(t->right->right, int));
}

void TreeTest::parents()
{
    fill();
    treeRRot(&t, t->left);
    treeLRot(&t, t->right);
    treeLRot(&t, t);

    for (Tree it = treeBegin(t);
         it != NULL;
         it = treeNext(it))
    {
        if (it->left)
            CPPUNIT_ASSERT(it->left->parent == it);
        if (it->right)
            CPPUNIT_ASSERT(it->right->parent == it);
    }
}

static void mult(void* a, void* b)
{
    int* A = (int*)a;
    int* B = (int*)b;

    *A *= *B;
}
void TreeTest::foreach()
{
    fill();
    int x = 2;

    treeForeach(t, mult, (void*)&x);

    CPPUNIT_ASSERT(t->left->left->right->parent->parent->parent == t);
    CPPUNIT_ASSERT_EQUAL(10, treeVal(t, int));
    CPPUNIT_ASSERT_EQUAL(6, treeVal(t->left, int));
    CPPUNIT_ASSERT(t->left->right == NULL);
    CPPUNIT_ASSERT_EQUAL(-16, treeVal(t->left->left, int));
    CPPUNIT_ASSERT(t->left->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(0, treeVal(t->left->left->right, int));
    CPPUNIT_ASSERT(t->left->left->right->left == NULL);
    CPPUNIT_ASSERT_EQUAL(2, treeVal(t->left->left->right->right, int));
    CPPUNIT_ASSERT(t->left->left->right->right->left == NULL);
    CPPUNIT_ASSERT(t->left->left->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(36, treeVal(t->right, int));
    CPPUNIT_ASSERT_EQUAL(14, treeVal(t->right->left, int));
    CPPUNIT_ASSERT(t->right->left->left == NULL);
    CPPUNIT_ASSERT_EQUAL(30, treeVal(t->right->left->right, int));
    CPPUNIT_ASSERT(t->right->left->right->left == NULL);
    CPPUNIT_ASSERT(t->right->left->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(200, treeVal(t->right->right, int));
    CPPUNIT_ASSERT(t->right->right->right == NULL);
    CPPUNIT_ASSERT_EQUAL(66, treeVal(t->right->right->left, int));
    CPPUNIT_ASSERT(t->right->right->left->left == NULL);
    CPPUNIT_ASSERT(t->right->right->left->right == NULL);

}
